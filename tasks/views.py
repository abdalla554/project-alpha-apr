from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, Task
from django.urls import reverse


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("list_projects"))
    else:
        form = TaskForm()
    return render(request, "tasks/createtask.html", {"form": form})


def show_my_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    return render(request, "tasks/tasks.html", {"tasks": tasks})
